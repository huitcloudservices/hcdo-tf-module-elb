# Module for custom TCP ELB's of a single port
resource "aws_security_group" "elb_security_group" {
  name_prefix = "${lower(var.product)}-${var.environment}-${var.app_scope}-elb-sg-"
  description = "Allow TCP ${var.listen_port} from ${join(", ", var.permitted_cidr)} Networks"
  vpc_id      = "${var.vpc_id}"
  count       = "${var.count}"

  ingress {
    from_port   = "${var.listen_port}"
    to_port     = "${var.listen_port}"
    protocol    = "tcp"
    cidr_blocks = ["${var.permitted_cidr}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name         = "${lower(var.product)}-${var.environment}-${var.app_scope}-elb-sg"
    environment  = "${lookup(var.environment_map, lower(var.environment))}"
    product      = "${var.product}"
    huit_assetid = "${var.huit_assetid}"
  }
}

resource "aws_elb" "elb" {
  name                        = "${lower(var.product)}-${lower(var.environment)}-${lower(var.app_scope)}-elb"
  security_groups             = ["${aws_security_group.elb_security_group.id}"]
  subnets                     = ["${var.elb_subnets}"]
  internal                    = "true"
  cross_zone_load_balancing   = "true"
  connection_draining         = "true"
  connection_draining_timeout = "120"
  count                       = "${var.count}"

  health_check {
    healthy_threshold   = "${var.healthy_threshold}"
    unhealthy_threshold = "${var.unhealthy_threshold}"
    target              = "${var.health_check_target}${var.health_check_path}"
    timeout             = "${var.health_check_timeout}"
    interval            = "${var.health_check_interval}"
  }

  listener {
    instance_port     = "${var.instance_port}"
    instance_protocol = "HTTP"
    lb_port           = "${var.listen_port}"
    lb_protocol       = "HTTP"
  }

  tags {
    Name         = "${lower(var.product)}-${var.environment}-${var.app_scope}-elb"
    product      = "${var.product}"
    huit_assetid = "${var.huit_assetid}"
    environment  = "${lookup(var.environment_map, lower(var.environment))}"
  }
}
