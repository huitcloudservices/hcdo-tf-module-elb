# Manditory variables for all modules
variable "huit_assetid" {}
variable "product" {}
variable "count" {
  default = "1"
}
variable "environment" {}
variable "environment_map" {
  type    = "map"
  default = {
    dev   = "Development"
    test  = "Testing"
    stage = "Staging"
    prod  = "Production"
  }
}

# Required Data Providers
data "aws_availability_zones" "available" {}

variable "app_scope" {
  description = "Application scope. i.e. app, node, master, slave, etc..."
  default     = "app"
}

# AWS Infrastructure Info
variable "vpc_id" {}
variable "elb_subnets" {
  type = "list"
}

# Adding to allow for module dependancy - see https://github.com/hashicorp/terraform/issues/1178#issuecomment-224782073
variable "wait_on" {
  description = "Variable to hold outputs of other moudles to force waiting"
  default     = "Nothing"
}

variable "health_check_target" {
  type = "string"
}
variable "health_check_path" {
  default = ""
}

variable "listen_port" {
  type = "string"
  description = "Port to listen on for ELB Externally"
}

variable "instance_port" {
  type = "string"
  description = "Port to communicate with the instances"
}

variable "healthy_threshold" {
  default = "5"
}
variable "unhealthy_threshold" {
  default = "5"
}
variable "health_check_timeout" {
  default = "10"
}
variable "health_check_interval" {
  default = "30"
}

variable "permitted_cidr" {
  type = "list"
  default = ["0.0.0.0/0"]
}
