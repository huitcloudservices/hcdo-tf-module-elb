output "elb_id" { value = "${aws_elb.elb.id}" }
output "dns_name" { value = "${aws_elb.elb.dns_name}"}
output "elb_sg_id" { value = "${aws_security_group.elb_security_group.id}" }
output "elb_name" { value = "${aws_elb.elb.name}" }
output "elb_zone_id" { value = "${aws_elb.elb.zone_id}" }