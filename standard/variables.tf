# Manditory variables for all modules
variable "huit_assetid" {
  type = "string"
}
variable "product" {
  type = "string"
}
variable "count" {
  default = "1"
}
variable "environment" {
  type = "string"
}
variable "environment_map" {
  type    = "map"
  default = {
    dev   = "Development"
    test  = "Testing"
    stage = "Staging"
    prod  = "Production"
  }
}

# Required Data Providers
data "aws_availability_zones" "available" {}

variable "app_scope" {
  description = "Application scope. i.e. app, node, master, slave, etc..."
  default     = "app"
  type        = "string"
}
# AWS Infrastructure Info
variable "vpc_id" {
  type = "string"
}
variable "elb_subnets" {
  type = "list"
}

# Adding to allow for module dependancy - see https://github.com/hashicorp/terraform/issues/1178#issuecomment-224782073
variable "wait_on" {
  description = "Variable to hold outputs of other moudles to force waiting"
  default     = "Nothing"
  type        = "string"
}

variable "health_check_target" {
  type = "string"
}
variable "health_check_path" {
  default = ""
  type    = "string"
}

variable "healthy_threshold" {
  default = "5"
  type    = "string"
}
variable "unhealthy_threshold" {
  default = "5"
  type    = "string"
}
variable "health_check_timeout" {
  default = "10"
  type    = "string"
}
variable "health_check_interval" {
  default = "30"
  type    = "string"
}

variable "permitted_cidr" {
  type = "list"
  default = ["0.0.0.0/0"]
}
variable "iam_sslcert_arn" {
  type = "string"
  description = "IAM SSL Cert ARN for External Port"
}
variable "client_ssl_encryption_map" {
  type = "map"
  default = {
    "true"  = "443,HTTPS"
    "false" = "80,HTTP"
  }
}
variable "client_ssl_encryption" {
  type = "string"
  default = "false"
  description = "Enable encryption to Instance from ELB"
}

variable "http_listener_instance_port" {
  default     = 80
  description = "The port listenting on the instance to route HTTP traffic to"
}
