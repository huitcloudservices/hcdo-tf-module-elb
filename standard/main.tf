# Module for Standard Private ELB's with 80/443
resource "aws_security_group" "elb_security_group" {
  name_prefix = "${lower(var.product)}-${var.environment}-${var.app_scope}-elb-sg-"
  description = "Allow 80 and 443 from ${join(", ", var.permitted_cidr)} Networks"
  vpc_id      = "${var.vpc_id}"
  count       = "${var.count}"


  ingress {
    from_port   = "80"
    to_port     = "80"
    protocol    = "tcp"
    cidr_blocks = ["${var.permitted_cidr}"]
  }

  ingress {
    from_port   = "443"
    to_port     = "443"
    protocol    = "tcp"
    cidr_blocks = ["${var.permitted_cidr}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name         = "${lower(var.product)}-${var.environment}-${var.app_scope}-elb-sg"
    environment  = "${lookup(var.environment_map, lower(var.environment))}"
    product      = "${var.product}"
    huit_assetid = "${var.huit_assetid}"
  }
}

resource "aws_elb" "elb" {
  name                        = "${lower(var.product)}-${lower(var.environment)}-${lower(var.app_scope)}-elb"
  security_groups             = ["${aws_security_group.elb_security_group.id}"]
  subnets                     = ["${var.elb_subnets}"]
  internal                    = "true"
  cross_zone_load_balancing   = "true"
  connection_draining         = "true"
  connection_draining_timeout = "120"
  count                       = "${var.count}"

  health_check {
    healthy_threshold   = "${var.healthy_threshold}"
    unhealthy_threshold = "${var.unhealthy_threshold}"
    target              = "${var.health_check_target}${var.health_check_path}"
    timeout             = "${var.health_check_timeout}"
    interval            = "${var.health_check_interval}"
  }

  listener {
    instance_port     = "${var.http_listener_instance_port}"
    instance_protocol = "HTTP"
    lb_port           = "80"
    lb_protocol       = "HTTP"
  }

    listener {
    instance_port      = "${element(split(",", lookup(var.client_ssl_encryption_map, var.client_ssl_encryption)), 0)}"
    instance_protocol  = "${element(split(",", lookup(var.client_ssl_encryption_map, var.client_ssl_encryption)), 1)}"
    lb_port            = "443"
    lb_protocol        = "HTTPS"
    ssl_certificate_id = "${var.iam_sslcert_arn}"
  }


  tags {
    Name         = "${lower(var.product)}-${lower(var.environment)}-${lower(var.app_scope)}-elb"
    product      = "${var.product}"
    huit_assetid = "${var.huit_assetid}"
    environment  = "${lookup(var.environment_map, lower(var.environment))}"
  }
}
