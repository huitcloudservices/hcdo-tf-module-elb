# ELB Terraform Modules

## Module Overview

There are four sub-modules in this module, each with a different purpose

|    Module Name    | Description                                                                        |
|:-----------------:|------------------------------------------------------------------------------------|
| standard          | An internal ELB with 80 for HTTP and 443 for HTTPS traffic                         |
| web-custom        | An internal ELB with a custom configured port for HTTP (non-secure) load balancing |
| secure-web-custom | An internal ELB with custom configured port for HTTPS (secure) load,balancing      |
| tcp-custom        | An internal ELB with a custom configured port for TCP load balancing               |

## Module Source Strings for Usage

*Note: There is a double slash (//) in the URI, that is required, see https://www.terraform.io/docs/modules/sources.html for more information*

#### Standard ELB

```
module "my_module_name" {
  source = "bitbucket.org/huitcloudservices/hcdo-tf-module-elb//standard"
  parameter = value 
  parameter = value
}
```
#### Web Custom ELB

```
module "my_module_name" {
  source = "bitbucket.org/huitcloudservices/hcdo-tf-module-elb//web-custom"
  parameter = value 
  parameter = value
}
```
#### Secure Web Custom ELB

```
module "my_module_name" {
  source = "bitbucket.org/huitcloudservices/hcdo-tf-module-elb//web-custom"
  parameter = value 
  parameter = value
}
```

#### TCP Custom ELB

```
module "my_module_name" {
  source = "bitbucket.org/huitcloudservices/hcdo-tf-module-elb//web-custom"
  parameter = value 
  parameter = value
}
```

## Required Parameters for All Sub-Modules

| Parameter             | Type   | Required? | Description                                                                                                               |
|:-----------------------:|:--------:|:-----------:|---------------------------------------------------------------------------------------------------------------------------|
| huit_assetid          | String |     Y     | 4 Digit Asset ID assigned by SLASH                                                                                        |
| product               | String |     Y     | Application name as it appears in SNOW                                                                                    |
| environment           | String |     Y     | Environment of the application - One of (dev, test, stage, prod)                                                         |
| function              | String |     Y     | Functional tier for the ELB                                                                                               |
| vpc_id                | String |     Y     | VPC ID for the ELB                                                                                                        |
| elb_subnets           |  List  |     Y     | List of subnets for the ELB to reside in                                                                                  |
| health_check_target   | String |     Y     | Evaluated Protocol and Port for Instance Health in the format "PROTOCOL:PORT" - Protocol is one of (HTTP, HTTPS, TCP, SSL)  |
| health_check_path     | String |     N*    | Required if health_check_target protocol is HTTP or HTTPS, should be the path to test for valid HTTP response such as "/" |
| health_check_timeout  | String |     N     | Timeout for health checks of instance, defaults to 10 seconds                                                             |
| health_check_interval | String |     N     | Interval for health checks of instance, must be longer than health_check_timeout -  Defaults to 30 seconds                   |
| permitted_cidr        | List   |     N     | List of permitted CIDR's which can access the ports for this ELB - defaults to all IP's                                   |



## Sub-module Specific Parameters



### standard

|       Parameter       |  Type  | Required? |                                              Description                                             |
|:---------------------:|:------:|:---------:|:---------------------------------------------------------------------------------------------------|
| iam_sslcert_arn       | String |     Y     | The ARN string for the SSL cert uploaded to AWS                                                      |
| client_ssl_encryption | String |     N     | "true" or "false" - if true HTTPS/443 will be used to communicate with the backend - Defaults to false |

### web-custom

|    Parameter    |  Type  | Required? | Description                                                                       |
|:---------------:|:------:|:---------:|-----------------------------------------------------------------------------------|
| listen_port     | String |     Y     | The port which the ELB should listen on                                           |
| instance_port   | String |     Y     | The port which the ELB communicates with the instances on                         |

### secure-web-custom

|    Parameter    |  Type  | Required? | Description                                                                       |
|:---------------:|:------:|:---------:|-----------------------------------------------------------------------------------|
| iam_sslcert_arn | String |     Y     | The ARN string for the SSL cert uploaded to AWS                                   |
| listen_port     | String |     Y     | The port which the ELB should listen on                                           |
| instance_port   | String |     Y     | The port which the ELB communicates with the instances on                         |
| client_protocol | String |     N     | The protocol the ELB uses to send to the client (HTTP or HTTPS), defaults to HTTP |

### tcp-custom

|    Parameter    |  Type  | Required? | Description                                                                       |
|:---------------:|:------:|:---------:|-----------------------------------------------------------------------------------|
| listen_port     | String |     Y     | The port which the ELB should listen on                                           |
| instance_port   | String |     Y     | The port which the ELB communicates with the instances on                         |


## Outputs from all sub-modules

| Output Name | Type   | Description                           |
|:-----------:|--------|---------------------------------------|
| elb_id      | String | AWS ID of the ELB                     |
| dns_name    | String | DNS record for the ELB                |
| elb_sg_id   | String | Security Group ID created for the ELB |
| elb_name    | String | Name of the ELB                       |
| elb_zone_id | String | ELB Zone ID for use in Route53        |